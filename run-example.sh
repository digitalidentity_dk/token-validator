#!/bin/bash
java -jar target/token-validator.jar test-data/valid-request-1.xml http://organisation.eksterntest-stoettesystemerne.dk/service/organisation/1 output-valid-1.txt
java -jar target/token-validator.jar test-data/valid-request-2.xml http://organisation.eksterntest-stoettesystemerne.dk/service/organisation/1 output-valid-2.txt
java -jar target/token-validator.jar test-data/invalid-request-1.xml http://organisation.eksterntest-stoettesystemerne.dk/service/organisation/1 output-invalid-1.txt
java -jar target/token-validator.jar test-data/invalid-request-2.xml http://organisation.eksterntest-stoettesystemerne.dk/service/organisation/1 output-invalid-2.txt
java -jar target/token-validator.jar test-data/invalid-request-3.xml http://organisation.eksterntest-stoettesystemerne.dk/service/organisation/1 output-invalid-3.txt
java -jar target/token-validator.jar test-data/invalid-request-4.xml http://organisation.eksterntest-stoettesystemerne.dk/service/organisation/1 output-invalid-4.txt
