package dk.digitalidentity;

import java.io.FileOutputStream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import dk.digitalidentity.tokenvalidator.CommandLineArguments;
import dk.digitalidentity.tokenvalidator.Validator;

@SpringBootApplication
public class Application {

	public static void main(String[] args) throws Exception {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(Application.class, args);
		boolean valid = false;
		String filePath = "error.log";

		try {
			Validator validator = applicationContext.getBean(Validator.class);
			CommandLineArguments arguments = applicationContext.getBean(CommandLineArguments.class);
			filePath = arguments.getOutputFilePath();
			valid = validator.validate(arguments);
		}
		catch (Exception ex) {
			System.out.println("Failed to run: " + ex.getMessage());
			valid = false;
		}

                FileOutputStream fos = new FileOutputStream(filePath);
                fos.write(((valid) ? "true" : "false").getBytes());
                fos.close();
	}
}
