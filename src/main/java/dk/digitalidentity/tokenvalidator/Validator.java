package dk.digitalidentity.tokenvalidator;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class Validator {
	final org.slf4j.Logger log = LoggerFactory.getLogger(Validator.class);

	@Autowired
	private ValidationRules validationRules;

	public boolean validate(CommandLineArguments arguments) {
		boolean result = true;

		String xml = "";
		//read all text from input file
		try (BufferedReader content = Files.newBufferedReader(Paths.get(arguments.getSoapFilePath()), Charset.forName("UTF-8"))) {
			for (String line; (line = content.readLine()) != null; xml += line);
		} catch (IOException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}

		result &= validationRules.isXmlWellFormed(xml);
		result &= validationRules.containsRequiredHeaders(xml);
		result &= validationRules.securityHeaderIsValid(xml);
		result &= validationRules.validateAssertion(xml, arguments.getEntityId());

		return result;
	}
}
