package dk.digitalidentity.tokenvalidator;

import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.common.saml.SAMLKeyInfo;
import org.apache.wss4j.common.saml.SamlAssertionWrapper;
import org.apache.wss4j.dom.WSDocInfo;
import org.apache.wss4j.dom.WSSConfig;
import org.apache.wss4j.dom.handler.RequestData;
import org.apache.wss4j.dom.saml.WSSSAMLKeyInfoProcessor;
import org.opensaml.saml2.core.Audience;
import org.opensaml.saml2.core.AudienceRestriction;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;


@Component
public class ValidationRules {
	final org.slf4j.Logger log = LoggerFactory.getLogger(ValidationRules.class);

	public boolean isXmlWellFormed(String xml) {
		boolean valid = true;

		try {
			getDocument(xml);
		}
		catch (Exception e) {
			valid = false;
		}

		return valid;
	}

	public boolean containsRequiredHeaders(String xml) {
		boolean valid = true;

		try {
			Document document = getDocument(xml);
			XPath xPath = XPathFactory.newInstance().newXPath();
			return (((NodeList) xPath.compile("/Envelope/Header/To").evaluate(document, XPathConstants.NODESET)).getLength() != 0) && (
					((NodeList) xPath.compile("/Envelope/Header/ReplyTo").evaluate(document, XPathConstants.NODESET)).getLength() != 0) && (
					((NodeList) xPath.compile("/Envelope/Header/MessageID").evaluate(document, XPathConstants.NODESET)).getLength() != 0) && (
					((NodeList) xPath.compile("/Envelope/Header/Action").evaluate(document, XPathConstants.NODESET)).getLength() != 0);
		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
			valid = false;
		}

		return valid;
	}

	public boolean securityHeaderIsValid(String xml) {
		try {
			Document document = getDocument(xml);

			// Timestamp element should exist and contain the elements 'created' and 'expired'
			XPath xPath = XPathFactory.newInstance().newXPath();
			if (((NodeList) xPath.compile("/Envelope/Header/Security").evaluate(document, XPathConstants.NODESET)).getLength() == 0
					|| ((NodeList) xPath.compile("/Envelope/Header/Security/Timestamp").evaluate(document, XPathConstants.NODESET)).getLength() == 0 || (
					((NodeList) xPath.compile("/Envelope/Header/Security/Timestamp/Created").evaluate(document, XPathConstants.NODESET)).getLength() == 0) || (
					((NodeList) xPath.compile("/Envelope/Header/Security/Timestamp/Expires").evaluate(document, XPathConstants.NODESET)).getLength() == 0)) {
				return false;
			}

			// CURRENT_TIMESTAMP must be between 'created' and 'expired'
			Object createdTest = xPath.compile("/Envelope/Header/Security/Timestamp/Created").evaluate(document, XPathConstants.NODE);
			Object sdsd = createdTest.getClass().getSimpleName();
			Node created = (Node) xPath.compile("/Envelope/Header/Security/Timestamp/Created").evaluate(document, XPathConstants.NODE);
			Node expires = (Node) xPath.compile("/Envelope/Header/Security/Timestamp/Expires").evaluate(document, XPathConstants.NODE);

			Instant currentTS = Instant.now();
			Instant createdTS = Instant.parse(created.getFirstChild().getNodeValue());
			Instant expiredTS = Instant.parse(expires.getFirstChild().getNodeValue());

			if (!(currentTS.isAfter(createdTS) && currentTS.isBefore(expiredTS))) {
				log.error("Invalid timestamp date ");
				return false;
			}

			// Security element must contain a Signature and Assertion element
			if (((NodeList) xPath.compile("/Envelope/Header/Security/Signature").evaluate(document, XPathConstants.NODESET)).getLength() == 0
					|| ((NodeList) xPath.compile("/Envelope/Header/Security/Assertion").evaluate(document, XPathConstants.NODESET)).getLength() == 0) {
				return false;
			}

		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return true;
	}

	public boolean validateAssertion(String xml, String entityId) {
		try {
			int startIndex = xml.indexOf("Assertion ");
			int endIndex = xml.indexOf("Assertion>", startIndex+1)+"Assertion>".length();
			while(xml.charAt(startIndex)!='<'){
				startIndex--;
			}
			xml = xml.substring(startIndex, endIndex);
			Document document = getDocumentNSAware(xml);

			// wrap the token in a SamlAssertionWrapper (as it has a lot of nice methods for validating the token)
			SamlAssertionWrapper samlAssertionWrapper = new SamlAssertionWrapper(document.getDocumentElement());

			// validate that the token is well-formed
			samlAssertionWrapper.validateAssertion(true);

			// validate that the token has not expired (defaults to 8 hours time-to-live if no expire timestamp is present in the token)
			samlAssertionWrapper.checkConditions(8 * 60 * 60);

			// validate that this token is indeed intended for this service (and not some other service)
			List<AudienceRestriction> audienceRestrictions = samlAssertionWrapper.getSaml2().getConditions().getAudienceRestrictions();
			boolean found = false;
			for (AudienceRestriction audienceRestriction : audienceRestrictions) {
				for (Audience audience : audienceRestriction.getAudiences()) {
					if (entityId.equals(audience.getAudienceURI())) {
						found = true;
					}
				}
			}

			if (!found) {
				throw new WSSecurityException(WSSecurityException.ErrorCode.FAILED_CHECK, "token audience does not match '" + entityId + "'");
			}

		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
			return false;
		}

		// the token passed all the validations
		return true;
	}

	public String nodeToString(Node node) {
		String xml = null;
		try {
			StringWriter writer = new StringWriter();
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform(new DOMSource(node), new StreamResult(writer));
			xml = writer.toString();
		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return xml;
	}

	private static Document getDocument(String xml) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);

		DocumentBuilder builder = factory.newDocumentBuilder();

		return builder.parse(new ByteArrayInputStream(xml.getBytes("utf-8")));
	}

	private static Document getDocumentNSAware(String xml) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		factory.setNamespaceAware(true);

		DocumentBuilder builder = factory.newDocumentBuilder();

		return builder.parse(new ByteArrayInputStream(xml.getBytes("utf-8")));
	}
}
