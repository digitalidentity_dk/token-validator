package dk.digitalidentity.tokenvalidator;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class CommandLineArguments {
	private String soapFilePath;
	private String entityId;
	private String outputFilePath;

	@Autowired
	private Environment env;

	@PostConstruct
	private void init() {
		if (env.getProperty("nonOptionArgs") == null) {
			throw new BeanCreationException("Missing parameters. Syntax : tokenvalidator <soap-file> <entityId> <output file path>");
		}

		String[] nonOptionArgs = env.getProperty("nonOptionArgs").split(",");
		if (nonOptionArgs.length != 3) {
			throw new BeanCreationException("Missing parameters. Syntax : tokenvalidator <token-file> <entityId> <output file path>");
		}

		soapFilePath = nonOptionArgs[0];
		entityId = nonOptionArgs[1];
		outputFilePath = nonOptionArgs[2];
	}
}
